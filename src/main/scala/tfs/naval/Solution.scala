package tfs.naval

import tfs.naval.field.FieldController
import tfs.naval.fleet.FleetController
import tfs.naval.model.{Game, Ship}
import tfs.naval.ship.ShipController

class Solution(shipController: ShipController, fieldController: FieldController, fleetController: FleetController) {

  def tryAddShip(game: Game, name: String, ship: Ship): Game = game match {
    case (field, fleet) if shipController.validateShip(ship) && fieldController.validatePosition(ship, field) =>
      fieldController.markUsedCells(field, ship) -> fleetController.enrichFleet(fleet, name, ship)
    case _ => game
  }
}